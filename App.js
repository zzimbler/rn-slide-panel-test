import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { Animated, StyleSheet, Text, View, Button, Dimensions } from 'react-native';
import SlidingUpPanel from 'rn-sliding-up-panel';

const { height, width } = Dimensions.get('window');
        /*onDragEnd={this.onDragEnd}
        showBackdrop={false}
        allowDragging={allowDragging}
        height={height-120}
        draggableRange={{top: height-120, bottom:0}}
        ref={this.setSlidePaneRef}
        onBottomReached={onClose}
        animatedValue={this._animatedValue}*/
export default function App() {
  const [ panel, setPanel ] = useState();
  const [ panel2, setPanel2 ] = useState();
  const [ panel3, setPanel3 ] = useState();
  const animatedValue = new Animated.Value(0);  // <-- Add this line
  const animatedValue2 = new Animated.Value(0);  // <-- Add this line
  const animatedValue3 = new Animated.Value(0);  // <-- Add this line

  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <View style={{ margin: 40 }}>
        <Button title='Show panel1' onPress={() => panel.show()} />
        <Button title='Show panel2' onPress={() => panel2.show()} />
        <Button title='Show panel3' onPress={() => panel3.show()} />
      </View>
      <SlidingUpPanel
        ref={(c) => {setPanel(c)}}
        height={height-120}
        animatedValue={animatedValue}
        draggableRange={{top: height-120, bottom:0}}
        friction={0}
      >
        <View style={styles.container}>
          <Text>1. Here is the content inside panel</Text>
          <Button title='Hide' onPress={() => panel.hide()} />
        </View>
      </SlidingUpPanel>
      <SlidingUpPanel
        ref={(c) => {setPanel2(c)}}
        height={height-120}
        animatedValue={animatedValue2}
        draggableRange={{top: height-120, bottom:0}}
        friction={0.5}
      >
        <View style={styles.container}>
          <Text>2. Here is the content inside panel</Text>
          <Button title='Hide' onPress={() => panel2.hide()} />
        </View>
      </SlidingUpPanel>
      <SlidingUpPanel
        ref={(c) => {setPanel3(c)}}
        height={height-120}
        animatedValue={animatedValue3}
        draggableRange={{top: height-120, bottom:0}}
      >
        <View style={styles.container}>
          <Text>3. Here is the content inside panel</Text>
          <Button title='Hide' onPress={() => panel3.hide()} />
        </View>
      </SlidingUpPanel>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
});
